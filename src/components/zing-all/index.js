import React, { Component, useState } from 'react';
import styles from '../../components/zing-all/style.css';


const Zing = props => {
    const[isOpenDropDown, setOpenDropDown] = useState(false);

    function handleClick(){
        setOpenDropDown(!isOpenDropDown);
    }

    return (
        <div className="item">
            <div className="item-id">{props.id}</div>
            <div className="item-img">
                <img src={props.image} alt={props.title}/>
            </div>
            <div className="item-info">
                <h2 className="title">{props.title}</h2>
                <div className="author">{props.author}</div>
            </div>
            <div className="item-time">{props.time}</div>
            <div className="extension">
                <div className="list-buttons">
                    <ul>
                        <li><i className="fa fa-microphone" aria-hidden="true"></i></li>
                        <li><i className="fa fa-heart-o" aria-hidden="true"></i></li>
                        <li onClick={() => {handleClick()}}><i className="dot"></i></li>
                    </ul>

                    {isOpenDropDown && <div className="dropdown-menu-list">
                            <div>
                                <div className="song-infor">
                                    <span className="thumb"><img src={props.image} alt={props.title}/></span>
                                    <div className="card-infor">
                                        <div className="title">{props.title}</div>
                                        <div className="log-stats">
                                            <div className="view">
                                                <i className="fa fa-microphone" aria-hidden="true"></i> {props.view}
                                            </div>
                                            <div className="like">    
                                                <i className="fa fa-heart-o" aria-hidden="true"></i> {props.like}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="block-menu ">
                                    <a className="block-menu-link z-disable">
                                        <i className="fa fa-download" aria-hidden="true"></i>
                                        <span>tải xuống</span>
                                    </a>
                                    <a className="block-menu-link">
                                        <i className="fa fa-link" aria-hidden="true"></i>
                                        <span>sao chép link</span>
                                    </a>
                                    <a className="block-menu-link">
                                        <i className="fa fa-minus-circle" aria-hidden="true"></i>
                                        <span>chặn</span>
                                    </a>
                                </div>

                                <ul className="block-item">
                                    <li className="list-item">
                                        <i className="fa fa-forward" aria-hidden="true"></i>
                                        <span>Thêm vào danh sách phát </span>
                                    </li>

                                    <li className="list-item">
                                        <i className="fa fa-play" aria-hidden="true"></i>
                                        <span>Phát tiếp theo</span>
                                    </li>

                                    <li className="list-item">
                                        <i className="fa fa-step-forward" aria-hidden="true"></i>
                                        <span>Thêm vào playlist</span>
                                    </li>

                                    <li className="list-item">
                                        <i className="fa fa-comment-o" aria-hidden="true"></i>
                                        <span>Bình luận</span>
                                    </li>

                                    <li className="list-item">
                                        <i className="fa fa-file-text-o" aria-hidden="true"></i>
                                        <span>Đóng góp lời bài hát</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    }
                </div>
            </div>

        </div>
    );
}
export default Zing;