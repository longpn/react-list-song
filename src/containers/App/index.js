import React from 'react';
import './App.css';
//import { Container, Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import Zing from '../../components/zing-all'
import data from '../../data/data.json'

function App() {
  return (
    <div className="wrapper_main">
      <div className="container">
        <div className="row">
        {
          data.items.map((items,i)=>{
            return(
              <Zing id={items.id} title={items.title} image={items.image} author={items.author} time={items.time} view={items.view} like={items.like}></Zing>
            );
          })
        }
        </div>
      </div>
    </div>
  );
}

export default App;
